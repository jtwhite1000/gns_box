import os
import numpy as np
import pandas as pd
import flopy
import pyemu
try:
   os.remove('box.list')
except Exception as e:
   print('error removing tmp file:box.list')
try:
   os.remove('box.hds')
except Exception as e:
   print('error removing tmp file:box.hds')
pyemu.helpers.apply_bc_pars()

pyemu.helpers.apply_array_pars()

pyemu.helpers.run('mf2005 box.nam 1>box.nam.stdout 2>box.nam.stderr')
pyemu.gw_utils.apply_mflist_budget_obs('box.list',flx_filename='flux.dat',vol_filename='vol.dat',start_datetime='1-1-1970')
pyemu.gw_utils.apply_hds_obs('box.hds')
