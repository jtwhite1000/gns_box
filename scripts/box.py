import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import flopy
import pyemu


NAM_FILE = "box.nam"
MODEL_WS = os.path.join("..","model")
COV_NAME = os.path.join(MODEL_WS, "truth.cov")

def make_base_model():
    m = flopy.modflow.Modflow(NAM_FILE.split('.')[0], version="mfnwt",
                              exe_name="mfnwt",
                              model_ws=MODEL_WS,
                              external_path=".")

    perlen = [1.0,100.0]
    steady = [True,False]



    flopy.modflow.ModflowDis(m,nlay=1,nrow=100,ncol=75,delr=100,delc=100,
                             perlen=perlen,steady=steady,nper=2,botm=-100,top=0.0)

    flopy.modflow.ModflowBas(m,strt=0.0)

    flopy.modflow.ModflowLpf(m,hk=3.0,sy=0.1,ss=0.0001,laytyp=0)


    drn_data = {0:[]}
    for i in range(75,99):
        drn_data[0].append([0,i,0,-10.0,4.0])
    flopy.modflow.ModflowDrn(m,stress_period_data=drn_data)


    riv_data = {0:[]}
    for j in range(m.ncol):
        riv_data[0].append([0,m.nrow-1,j,-10.0,4.5,-15.0])
    flopy.modflow.ModflowRiv(m,stress_period_data=riv_data)

    flopy.modflow.ModflowRch(m,rech={0:0.000035,1:0.0000035})

    wel_data = {0:[],1:[]}
    wel_is = [60,80]
    wel_j = 20
    for wi in wel_is:
        wel_data[0].append([0,wi,wel_j,-450.0])

    for wi in wel_is:
        wel_data[1].append([0,wi,wel_j,-550.0])
    flopy.modflow.ModflowWel(m,stress_period_data=wel_data)

    #flopy.modflow.ModflowNwt(m)
    flopy.modflow.ModflowPcg(m)

    oc_data = {(0,0):["save head","save budget"]}
    oc_data[(1,0)] = ["save head","save budget"]

    flopy.modflow.ModflowOc(m,stress_period_data=oc_data)

    m.write_input()

    pyemu.helpers.run("mfnwt {0}".format(NAM_FILE),cwd=MODEL_WS)

    hds = flopy.utils.HeadFile(os.path.join(m.model_ws,"{0}.hds".format(m.name)))
    p = plt.imshow(hds.get_data()[0])
    plt.colorbar(p)

    plt.savefig("hds.base.png")
    plt.close("all")

    return m


def _build_cov(m):
    d = []
    for i in range(m.nrow):
        for j in range(m.ncol):
            d.append(["i{0:03d}j{1:03d}".format(i, j), m.sr.xcentergrid[i, j], m.sr.ycentergrid[i, j]])
    df = pd.DataFrame(d, columns=["name", "x", "y"])

    gs = pyemu.geostats.GeoStruct(variograms=[pyemu.geostats.ExpVario(0.25, a=2000, anisotropy=4.0, bearing=45.0)])
    cov = gs.covariance_matrix(df.x, df.y, names=df.name)
    cov.to_binary(COV_NAME)
    return cov


def make_truth(m):
    cov_name = os.path.join(m.model_ws,"truth.cov")
    if not os.path.exists(cov_name):
        _build_cov(m)

    cov = pyemu.Cov.from_binary(COV_NAME)

    real = cov.draw(np.log10(m.lpf.hk.array.mean()))
    real = 10.0**real

    real = real.reshape(m.nrow,m.ncol)
    plt.imshow(np.log10(real))
    plt.savefig("truth.hk.png")
    #np.savetxt(os.path.join(m.model_ws,"hk_layer_1.ref"))
    m.lpf.hk[0] = real

    rarr = m.rch.rech[0].array
    rarr[:50,-30:] *= 1.75
    rarr[-15:,:] *= 0.2
    plt.imshow(rarr)
    plt.savefig("truth.rch.png")
    m.rch.rech[0] = rarr

    m.write_input()

    pyemu.helpers.run("mfnwt {0}".format(NAM_FILE), cwd=MODEL_WS)

    hds = flopy.utils.HeadFile(os.path.join(m.model_ws, "{0}.hds".format(m.name)))
    p = plt.imshow(hds.get_data()[0])
    plt.colorbar(p)

    plt.savefig("hds.truth.png")
    plt.close("all")

def setup_hds_obs():
    m = flopy.modflow.Modflow.load(NAM_FILE, model_ws=MODEL_WS, check=False)

    hds = flopy.utils.HeadFile(os.path.join(m.model_ws, "{0}.hds".format(m.name)))
    p = plt.imshow(hds.get_data()[0])
    plt.colorbar(p)

    plt.savefig("hds.truth.png")
    plt.close("all")

    p= plt.imshow(np.log10(m.lpf.hk[0].array))
    plt.colorbar(p)
    plt.savefig("hk.truth.png")
    plt.close("all")

    pyemu.gw_utils.setup_hds_obs(os.path.join(MODEL_WS,NAM_FILE.replace(".nam",".hds")),
                                 kperk_pairs=[[0,0],[1,0]])

    obs_js = [30,60]
    obs_is = [20,40,60,80]
    df = pd.read_csv(os.path.join(MODEL_WS,"_setup_box.hds.csv"))
    df.index = df.obsnme
    obsnme = df.loc[df.apply(lambda x: x.iidx in obs_is and x.jidx in obs_js and x.kper==0,axis=1),"obsnme"]
    print(obsnme)
    df.loc[:,"weight"] = 0.0
    df.loc[obsnme,"weight"] = 2.0
    df.to_csv(os.path.join(MODEL_WS,"_setup_box.hds.csv"))


def setup_flux_obs():
    m = flopy.modflow.Modflow.load(NAM_FILE, model_ws=MODEL_WS, check=False)
    #mfl = flopy.utils.MfListBudget(os.path.join(m.model_ws,m.namefile.replace("")))
    flx,vol = pyemu.gw_utils.apply_mflist_budget_obs(os.path.join(m.model_ws,"box.list"))

    flx.to_csv(os.path.join(m.model_ws,"_setup_box.list.csv"))

if __name__ == "__main__":
    #make_base_model()
    #_build_cov(make_base_model())
    #make_truth(make_base_model())
    setup_hds_obs()
    setup_flux_obs()

